import React, {useMemo} from 'react';

import Table from './Table';

function App({ data = [], columns = []}) {
  const tableColumns = useMemo(
    () => columns
  ,[columns]);

  const tableData = useMemo(
    () => data
  , [data])

  return (
    <div className="App">
      <Table columns={tableColumns} data={tableData} />
    </div>
  )
}

export default App;
