export const MEDIAN = "MEDIAN";
export const COLUMN_TYPE_MEDIAN = [
  {
    Header: 'Box',
    accessor: 'id',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Sensor Type',
    accessor: 'sensor_type',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Median',
    accessor: 'median',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Measurement Unit',
    accessor: 'unit',
    disableSortBy: true,
    disableFilters: true
  }
];

export const ALL = "ALL";
export const COLUMN_TYPE_ALL = [
  {
    Header: 'Box',
    accessor: 'id',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Box ID',
    accessor: 'box_id',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Sensor Type',
    accessor: 'sensor_type',
  },
  {
    Header: 'Data Type',
    accessor: 'name',
    disableSortBy: true
  },
  {
    Header: 'Data Value',
    accessor: 'reading',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Measurement Unit',
    accessor: 'unit',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Lower Bound Range',
    accessor: 'range_l',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Upper Bound Range',
    accessor: 'range_u',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Longitude',
    accessor: 'longitude',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Latitude',
    accessor: 'latitude',
    disableSortBy: true,
    disableFilters: true
  },
  {
    Header: 'Date',
    accessor: 'reading_ts',
    disableFilters: true
  }
]

const boxTypes = [
  { id: "Box-A1-O3", sensor_type: "O3", unit: "ppm"},
  { id: "Box-A1-NO2", sensor_type: "NO2", unit: "ppm"},
  { id: "Box-A1-CO", sensor_type: "CO", unit: "ppm"},
  { id: "Box-A1-TEMP", sensor_type: "TEMP", unit: "\u00baC"},
  { id: "Box-A1-RH", sensor_type: "RH", unit: "%"},
  { id: "Box-A2-O3", sensor_type: "O3", unit: "ppm"},
  { id: "Box-A2-NO2", sensor_type: "NO2", unit: "ppm"},
  { id: "Box-A2-CO", sensor_type: "CO", unit: "ppm"},
  { id: "Box-A2-TEMP", sensor_type: "TEMP", unit: "\u00baC"},
  { id: "Box-A2-RH", sensor_type: "RH", unit: "%"},
  { id: "Box-B1-O3", sensor_type: "O3", unit: "ppm"},
  { id: "Box-B1-NO2", sensor_type: "NO2", unit: "ppm"},
  { id: "Box-B1-CO", sensor_type: "CO", unit: "ppm"},
  { id: "Box-B1-TEMP", sensor_type: "TEMP", unit: "\u00baC"},
  { id: "Box-B1-RH", sensor_type: "RH", unit: "%"},
  { id: "Box-B2-O3", sensor_type: "O3", unit: "ppm"},
  { id: "Box-B2-NO2", sensor_type: "NO2", unit: "ppm"},
  { id: "Box-B2-CO", sensor_type: "CO", unit: "ppm"},
  { id: "Box-B2-TEMP", sensor_type: "TEMP", unit: "\u00baC"},
  { id: "Box-B2-RH", sensor_type: "RH", unit: "%"}
]

export function calculateMedian(data) {
  const medianArr = boxTypes.map(box => {
    const filteredData = data.filter(item => item.id === box.id && item.sensor_type === box.sensor_type);
    
    const mid = Math.floor(filteredData.length / 2);
    const nums = [...filteredData].sort((a, b) => a.reading - b.reading);
    const median = filteredData.length % 2 !== 0 ? nums[mid].reading : (nums[mid].reading - 1 + nums[mid].reading) / 2;

    return {
      id: box.id,
      sensor_type: box.sensor_type,
      median,
      unit: box.unit
    }
  })

  return medianArr;
}