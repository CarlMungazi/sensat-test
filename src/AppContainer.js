import React, {useState } from 'react';
import Form from 'react-bootstrap/Form';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import ALL_DATA from './data.json';
import { calculateMedian, MEDIAN, ALL, COLUMN_TYPE_ALL, COLUMN_TYPE_MEDIAN } from './utils';
import App from './App';

const tableColumns = {
  COLUMN_TYPE_ALL,
  COLUMN_TYPE_MEDIAN
}

function AppContainer() {
  const [data, setData] = useState(ALL_DATA);
  const [medianData, setMedianData] = useState([]);
  const [columns, setCols] = useState(COLUMN_TYPE_ALL);

  return (
    <div className="AppContainer">
      <Form>
        <Form.Group controlId="tableview">
          <Form.Label>Select Table View</Form.Label>
          <Form.Control 
            as="select" 
            custom
            onChange={(evt) => {
              const columnType = `COLUMN_TYPE_${evt.target.value}`;
              setCols(tableColumns[columnType]);

              if (evt.target.value === MEDIAN) {
                // if we have this stored already, use it
                if (medianData.length > 0) {
                  setData(medianData);
                  return;
                }
                
                const calculatedMedianData = calculateMedian(ALL_DATA);
                // store this calculation in local state so we don't have to do it again
                setMedianData(calculatedMedianData);
                setData(calculatedMedianData);
                return;
              }

              setData(ALL_DATA)
            }}
          >
            <option value={ALL}>Default</option>
            <option value={MEDIAN}>Median Values</option>
          </Form.Control>
        </Form.Group>
      </Form>
      <App data={data} columns={columns}/>
    </div>
  );
}

export default AppContainer;
