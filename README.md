This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Project Explanation

### Reasons for library choice
- React - I am most productive with and more comfortable using React. However, in reality any framework (or even a vanilla JS solution) would suffice for this task, depending on the deadlines and product requirements
- React Table - the filtering, sorting and memoization functionality makes this library perfect for tabulating large data sets. Furthermore, it is a 'headless' UI library so it exposes all the necessary functionality and leaves the styling decisions to the developer
- React Bootstrap - if I want to quickly design a UI, I normally reach for [Tachyons](http://tachyons.io/) because its atomic CSS approach makes iterating much easier and faster. I used Bootstrap because React Table had a working example with Bootstrap, so it was the much quicker option. I tend to avoid Bootstrap because you quickly find yourself having to override the in-built classes and fighting with the framework when you want to build custom designs

### Reasons for approach
- AppContainer & App Component - It is always a good idea to organise your code into presentational and container components. Container components help you abstract away any logic related with data fetching and manipulation, leaving presentational components to worry about the UI itself
- Utils file - Data manipulation functionality such as the function for calculating the median values is best placed away from the components or, if you're using a state management library such as Redux, away from the reducer files. This makes it easier to test and makes the code easier to read
- Calculating the median value - I made the assumption that the data for the different box types and sensor types would be available beforehand. If this was not available, this would be a trickier and more costly calculation because it would require:
  - figuring out all the sensor types
  - figuring out all the combinations of sensor types and box types
  - filtering through the entire dataset and calculating the median values

### Improvements
- Testing - unit tests for any functionality in the utils file and end-to-end tests for the application itself using atool such as Cypress. You can also use a tool such as Percy to test the visual elements of the UI
- SASS - I would use SASS to organise the CSS and if time permitted, create reusable UI components not based on Bootstrap.
- Improved UX - Adding a loading animation before the table appears and when paginating would give the user a more delightful experience
- State management - In a production environment this data would be fetched over the network and given that it will most likely be manipulated in various ways, storing the original data in state would be useful.


## How to run the project

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.